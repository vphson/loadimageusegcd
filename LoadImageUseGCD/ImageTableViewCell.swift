//
//  ImageTableViewCell.swift
//  LoadImageUseGCD
//
//  Created by Hoang Son Vo Phuoc on 3/20/23.
//

import UIKit
import SnapKit

class ImageTableViewCell: UITableViewCell {

    static let cellID: String = "ImageTableViewCell"
    
    var imageRequest: ImageDownloader?
    
    private lazy var photoImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        imageRequest?.cancelRequest()
    }
    
    private func setupView() {
        contentView.addSubview(photoImageView)
        
        photoImageView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview().inset(12)
            make.bottom.equalToSuperview().offset(-12)
            make.height.equalTo(200)
        }
    }
    
    func setupViewModel(image: UIImage) {
        photoImageView.image = image
    }
}

class ImageDownloader {
    
    var task: URLSessionTask?
    
    var imageLoading: [String] = []
    
    func loadImage(_ imageURL: String,
                   completion: @escaping (UIImage) -> Void) {
        
        guard !imageLoading.contains(imageURL) else {
            debugPrint("image loading from ......")
            return 
        }
        
        let url = URL(string: imageURL)
        task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            
            DispatchQueue.main.async(execute: {
                if let downloadedImage = UIImage(data: data!) {
                    debugPrint("image downloaded from server... \(imageURL)")
                    self.imageLoading.append(imageURL)
                    completion(downloadedImage)
                }
            })
            
        })
        task?.resume()
    }
    
    func cancelRequest() {
        task?.cancel()
    }
    
    func isCancelRequest() -> Bool {
        task?.progress.isCancelled ?? false
    }
}
