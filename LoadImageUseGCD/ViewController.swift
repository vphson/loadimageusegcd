//
//  ViewController.swift
//  LoadImageUseGCD
//
//  Created by Hoang Son Vo Phuoc on 3/20/23.
//

import UIKit

class ViewController: UIViewController {

    let arrayImages: [String] = [
        "https://strgblobimagedemo.blob.core.windows.net/imagecontainer/bear.jpg",
        "https://strgblobimagedemo.blob.core.windows.net/imagecontainer/brd.jpg",
        "https://strgblobimagedemo.blob.core.windows.net/imagecontainer/btrfly.jpg",
        "https://strgblobimagedemo.blob.core.windows.net/imagecontainer/cat.jpg",
        "https://strgblobimagedemo.blob.core.windows.net/imagecontainer/che.jpg",
        "https://images.pexels.com/photos/1216482/pexels-photo-1216482.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/1851164/pexels-photo-1851164.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/1755226/pexels-photo-1755226.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/8780211/pexels-photo-8780211.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/1714454/pexels-photo-1714454.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11966610/pexels-photo-11966610.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/9687810/pexels-photo-9687810.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/5579257/pexels-photo-5579257.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/3038776/pexels-photo-3038776.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/1812172/pexels-photo-1812172.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/17811/pexels-photo.jpg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/1266446/pexels-photo-1266446.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/13995182/pexels-photo-13995182.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12057236/pexels-photo-12057236.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12932104/pexels-photo-12932104.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/6122633/pexels-photo-6122633.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11648506/pexels-photo-11648506.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12715113/pexels-photo-12715113.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12715115/pexels-photo-12715115.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12715119/pexels-photo-12715119.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/12568029/pexels-photo-12568029.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12568024/pexels-photo-12568024.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/7955515/pexels-photo-7955515.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/10849243/pexels-photo-10849243.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/8838177/pexels-photo-8838177.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/7154558/pexels-photo-7154558.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11735419/pexels-photo-11735419.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/6846275/pexels-photo-6846275.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/13919108/pexels-photo-13919108.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/15130001/pexels-photo-15130001.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11079402/pexels-photo-11079402.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/13995054/pexels-photo-13995054.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11468377/pexels-photo-11468377.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/15037523/pexels-photo-15037523.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/13919111/pexels-photo-13919111.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/14911667/pexels-photo-14911667.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/15078042/pexels-photo-15078042.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/7200719/pexels-photo-7200719.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11107937/pexels-photo-11107937.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/11098932/pexels-photo-11098932.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/15208071/pexels-photo-15208071.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/13200020/pexels-photo-13200020.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/12905388/pexels-photo-12905388.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/13779811/pexels-photo-13779811.jpeg?auto=compress&cs=tinysrgb&w=800&lazy=load",
        "https://images.pexels.com/photos/2808658/pexels-photo-2808658.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15858946/pexels-photo-15858946.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15920678/pexels-photo-15920678.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15852718/pexels-photo-15852718.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15859232/pexels-photo-15859232.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15846401/pexels-photo-15846401.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15902774/pexels-photo-15902774.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15893595/pexels-photo-15893595.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
        "https://images.pexels.com/photos/15846427/pexels-photo-15846427.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15848853/pexels-photo-15848853.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15858253/pexels-photo-15858253.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15852358/pexels-photo-15852358.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15928135/pexels-photo-15928135.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15850415/pexels-photo-15850415.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15853145/pexels-photo-15853145.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15854376/pexels-photo-15854376.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15857674/pexels-photo-15857674.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/15846854/pexels-photo-15846854.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14045068/pexels-photo-14045068.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14093489/pexels-photo-14093489.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14095723/pexels-photo-14095723.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/13471866/pexels-photo-13471866.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14118381/pexels-photo-14118381.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14104441/pexels-photo-14104441.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14079752/pexels-photo-14079752.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14092949/pexels-photo-14092949.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14054579/pexels-photo-14054579.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14036332/pexels-photo-14036332.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14126274/pexels-photo-14126274.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14108404/pexels-photo-14108404.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14113638/pexels-photo-14113638.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14095437/pexels-photo-14095437.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14122514/pexels-photo-14122514.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14118599/pexels-photo-14118599.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14092414/pexels-photo-14092414.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14095333/pexels-photo-14095333.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14112246/pexels-photo-14112246.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14091182/pexels-photo-14091182.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14100743/pexels-photo-14100743.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14101281/pexels-photo-14101281.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14108392/pexels-photo-14108392.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14111591/pexels-photo-14111591.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14108248/pexels-photo-14108248.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14100850/pexels-photo-14100850.jpeg?auto=compress&cs=tinysrgb&w=800",
        "https://images.pexels.com/photos/14108246/pexels-photo-14108246.jpeg?auto=compress&cs=tinysrgb&w=800",
    ]
    
    private let imageCache = NSCache<AnyObject, UIImage>()
    
    var imageRequest: ImageDownloader?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 167
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.contentInset = UIEdgeInsets(top: 12, left: 0, bottom: 0, right: 0)
        tableView.register(ImageTableViewCell.self, forCellReuseIdentifier: ImageTableViewCell.cellID)
        return tableView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

    }


    private func loadImageByIndex(_ indexPath: IndexPath, cell: ImageTableViewCell){
        
        let imageURL = arrayImages[indexPath.row]

        let semaphore = DispatchSemaphore(value: 1)
        
        semaphore.wait()
        
        let imageUpdatingCompletion: (_ image: UIImage) -> Void = { image in
            cell.setupViewModel(image: image)
        }
        
        if let cachedImage = imageCache.object(forKey: imageURL as AnyObject) {
            debugPrint("image loaded from cache for.....\(imageURL)")
            imageUpdatingCompletion(cachedImage)
        }

        imageRequest = ImageDownloader()
        
        imageRequest?.loadImage(arrayImages[indexPath.row]) { image in
            
            self.imageCache.setObject(image, forKey: imageURL as AnyObject)
     
            imageUpdatingCompletion(image)
        }
        
        tableView.reloadRows(at: [indexPath], with: .none)
        
        semaphore.signal()
    }

}

extension ViewController: UITableViewDelegate {
    
}

extension ViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayImages.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell = cell as! ImageTableViewCell
        
        loadImageByIndex(indexPath, cell: cell)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.cellID, for: indexPath) as! ImageTableViewCell
        cell.imageRequest = imageRequest
        return cell
    }
}

